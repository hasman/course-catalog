import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesComponent } from './components/courses/courses.component';
import { CoursesGetComponent } from './components/courses-get/courses-get.component';

const routes: Routes = [
  {
    path: '**',
    component: CoursesComponent 
  },
  {
    path: 'signup',
    component: CoursesComponent 
  },
  {
    path: 'courses',
    component: CoursesGetComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
