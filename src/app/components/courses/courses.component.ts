import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs-compat/operator/map';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../validators/mustmatch';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
  
})
export class CoursesComponent implements OnInit {

  registerForm: FormGroup;
    submitted = false;
    unamePattern = "^[a-z0-9_-]{8,15}$"; 
    subjectSelected = '';
    topicSelected = '';
    mathSelected = false;
    scienceSelected = false;
    artSelected = false;
    languageSelected = false;
    topics = [];
    timeslots = [];
//courses
    course = [];
    arrayCource = [];

    user = '';
    id = '';
    user_email = '';
    allcheck = true;

    constructor(private formBuilder: FormBuilder) {
      this.course = [
        {
          studentid: '11001100',
          username: 'ahmadou123',
          email: 'hasman16@gmail.com',
          subject: 'Math',
          topic: 'Algebra',
          time: '11:00 AM'

        },
        {
          studentid: '11001105',
          username: 'chris123',
          email: 'chris@urbaninsight.com',
          subject: 'Science',
          topic: 'Chemistry',
          time: '9:00 AM'

        },
        {
          studentid: '11001100',
          username: 'ahmadou',
          email: 'hasman16@gmail.com',
          subject: 'Language Arts',
          topic: 'Grammar',
          time: '10:00 AM'

        }
      ];
     }

    ngOnInit() {
       this.registerForm = this.formBuilder.group({
         
          username: ['', [Validators.required, Validators.minLength(8), Validators.pattern(this.unamePattern)]],
          
          email: ['', [Validators.required, Validators.email]],
          
          subject: ['', Validators.required],
          topic: ['', Validators.required],
          time: ['', Validators.required]
          //acceptTerms: [false, Validators.requiredTrue]
      }); 
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.arrayCource = this.registerForm.value;
//Push the new selection into the course list
//The student ID should be fetched based on the username
  this.arrayCource['studentid'] = '100110019';  
  this.arrayCource['username']= this.registerForm.value.username;
  this.arrayCource['email']= this.registerForm.value.email;
  this.arrayCource['subject']= this.registerForm.value.subject;
  this.arrayCource['topic']= this.registerForm.value.topic;
  this.arrayCource['time']= this.registerForm.value.time;

      this.course.push(this.arrayCource);


      this.id = this.arrayCource['studentid'];
      this.user = this.registerForm.value.username;
      this.user_email = this.registerForm.value.email;
      // display form values on success
      //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));

      //this.arrayCource.forEach((key : any, val: any) => {
        console.log('elt: ' +  JSON.stringify(this.course, null, 4));
        
     // });
      //console.log('course!! :-)\n\n' + this.registerForm.value);
      this.subjectSelected = '';
      this.topicSelected = '';
      this.topics = []
      this.timeslots = [];
  }

  onReset() {
      this.submitted = false;
      this.registerForm.reset();
  }

  //Subject
  public onOptionsSubjectSelected(event) {
    const value = event.target.value;
    this.subjectSelected = value;

    switch(this.subjectSelected){
    case 'Math':
      this.topics = ['Algebra', 'Trigonometry', 'Calculus'];
      
      break;
    case 'Science':
      this.topics = ['Physics', 'Chemistry', 'Biology'];  
      break;
    case 'Art':
      this.topics = ['Art History', 'Painting', 'Drawing'];  
        break;
      case 'Language Arts':
        this.topics = ['Literature', 'Grammar', 'Writing'];  
          break;

    }

    console.log('subjectSelected: ' + this.subjectSelected + '  value: ' +value);
 }

    //Topipc
  public onOptionsTopicSelected(event) {
      const value = event.target.value;
      this.topicSelected = value;
  
      switch(this.topicSelected){
        case 'Algebra':
          this.timeslots = ['8:00 AM', '11:00 AM'];    
          
          
          break;
        case 'Trigonometry':
          this.timeslots = ['9:00 AM', '12:00 PM'];  
          break;
        case 'Calculus':
          this.timeslots = ['10:00 AM', '3:00 PM'];  
            break;
        case 'Physics':
            this.timeslots = ['10:00 AM', '3:00 PM'];  
          break;
          case 'Chemistry':
          this.timeslots = ['9:00 AM', '1:00 PM'];  
          break;
        case 'Biology':
          this.timeslots = ['8:00 AM', '10:00 AM'];  
            break;
        case 'Art History':
            this.timeslots = ['11:00 AM'];  
          break;
          case 'Painting':
          this.timeslots = ['2:00 PM'];  
          break;
        case 'Drawing':
          this.timeslots = ['8:00 AM', '5:00 PM'];  
            break;
        case 'Literature':
            this.timeslots = ['8:30 AM', '11:45 AM'];  
          break;
          case 'Grammar':
          this.timeslots = ['8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM', '1:00 PM'];  
            break;
        case 'Writing':
            this.timeslots = ['8:00 AM', '11:00 AM'];  
          break;
    
        } 

      console.log('topicSelected: ' + this.topicSelected + '  value: ' +value);
    }
      //Timeslot
  public onOptionsTimeSelected(event) {
    const value = event.target.value;
    //Check if this timeslot is already taken by the same student
    //alert(value);
//alert('course: ' + this.course);
for(let key of this.course){
  if(key.time === value){
    alert('This timeslot is already taken, please choose a different time');
   
  } else{
    this.allcheck = false;
  }
  //console.log(key); 
}

  }
}