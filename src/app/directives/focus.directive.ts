import { Directive, Input, EventEmitter, ElementRef, Inject, Renderer2, AfterViewInit } from '@angular/core';


@Directive({
    selector: '[appFocus]'
})
export class FocusDirective implements AfterViewInit {
    @Input('appFocus') appFocus: EventEmitter<boolean>;

    constructor(@Inject(ElementRef) private element: ElementRef, private renderer: Renderer2) {
      
    }

    ngAfterViewInit() {
      this.appFocus.subscribe(event => {
        setTimeout(() => {
          this.renderer.selectRootElement(this.element.nativeElement).focus();
        });
      });
    }
}

